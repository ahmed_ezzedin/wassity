import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:wassity/core/models/user.dart';
import 'package:wassity/core/services/core_helper/preference.dart';

class AuthService extends ChangeNotifier {
  User? _user;
  User? get user => _user;
  bool get isLogin => Preference.getBool(PrefKeys.isUserLoged) != null;

  saveUser(User user) {
    Preference.setString(PrefKeys.User, json.encode(user.toJson()));
    Preference.setString(PrefKeys.token, "======== USER_TOKEN ========");
    Preference.setBool(PrefKeys.isUserLoged, true);
    _user = user;
  }

  loadUser() {
    Map<String, dynamic> userMap =
        json.decode(Preference.getString(PrefKeys.User)!);
    _user = User.fromJson(userMap);
  }

  Future<void> signOut() async {
    await Preference.clear();
    _user = null;
  }

  Future<void> signIn() async {
    _user = User(
      id: "test_id",
      createAt: 958351515,
      name: "ahmed ezzeldin",
      email: "ahmed@test.com",
      phone: "0123456789",
    );
  }
}
