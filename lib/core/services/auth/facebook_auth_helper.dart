import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class FacebookAuthHelper {
  static FacebookAuth _facebookAuth = FacebookAuth.instance;

  static Future<Map<String, dynamic>> getUserData() async {
    return _facebookAuth.getUserData();
  }

  static Future<void> signInWithFacebook() async {
    try {
      await _facebookAuth.login();
    } catch (error) {
      print(error);
    }
  }

  static Future<void> signOutWithGoogle() async {
    try {
      await _facebookAuth.logOut();
    } catch (error) {
      print(error);
    }
  }
}
