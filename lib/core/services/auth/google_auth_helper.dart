import 'package:google_sign_in/google_sign_in.dart';

class GoogleAuthHelper {
  static GoogleSignIn _googleSignIn = GoogleSignIn();

  static GoogleSignInAccount? get currentUser => _googleSignIn.currentUser;

  static Future<void> signInWithGoogle() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  static Future<void> signOutWithGoogle() async {
    try {
      //  await  _googleSignIn.signOut();
      await _googleSignIn.disconnect();
    } catch (error) {
      print(error);
    }
  }
}
