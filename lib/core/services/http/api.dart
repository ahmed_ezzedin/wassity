
enum RequestType {
  Post,
  Put,
  Get,
  Delete,
}

class Headers {
  static Map<String, String> get clientAuth => {
        "Authorization":
            "============================ Bearer Auth_Token ============================"
      };

  static Map<String, String> get userAuth => {
        "Authorization":
            "============================ Bearer User_Token ============================"
      };

  static Map<String, String> get baseHeader => {
        "content-type": "application/json",
        "Authorization": "key= AAAAAAAAAAAAAAAAAAA"
      };
}

class EndPoints {
  static const String ServerURL = "https://jsonplaceholder.typicode.com/";
  static const String Users = "users";
  static const String Posts = "posts";
}
