import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:wassity/core/services/http/api.dart';

class HttpService {
  // =========================================================== Main Request Function
  static Future<dynamic> _request({
    required String endPoint,
    required RequestType requestType,
    required Map<String, String> header,
    Map<String, dynamic>? body,
  }) async {
    http.Response? response;
    try {
      switch (requestType) {
        case RequestType.Post:
          {
            response = await http.post(
              Uri.parse(EndPoints.ServerURL + endPoint),
              headers: header,
              encoding: Encoding.getByName("utf-8"),
              body: json.encode(body),
            );
          }
          break;
        case RequestType.Put:
          {
            response = await http.put(
              Uri.parse(EndPoints.ServerURL + endPoint),
              headers: header,
              encoding: Encoding.getByName("utf-8"),
              body: json.encode(body),
            );
          }
          break;
        case RequestType.Get:
          {
            response = await http.get(
              Uri.parse(EndPoints.ServerURL + endPoint),
              headers: header,
            );
          }
          break;
        case RequestType.Delete:
          {
            response = await http.delete(
              Uri.parse(EndPoints.ServerURL + endPoint),
              headers: header,
            );
          }
          break;
      }
      if (response.statusCode == 200 || response.statusCode == 201) {
        print("Request Success ✔️");
      } else {
        print("Request Failed Code(${response.statusCode}) ❌");
      }
    } catch (error) {
      print("⚠️ ${error.toString()} ⚠️");
    }
    if (response != null) {
      return json.decode((response.body));
    } else {
      return null;
    }
  }

  // =========================================================== Get Users
  Future<dynamic> getUsers() async {
    final res = await _request(
      endPoint: EndPoints.Users,
      requestType: RequestType.Get,
      header: Headers.clientAuth,
    );
    return res;
  }

  // =========================================================== Delete User
  Future<dynamic> deleteUser(int id) async {
    final res = await _request(
      endPoint: EndPoints.Users + "/$id",
      requestType: RequestType.Delete,
      header: Headers.baseHeader,
    );
    return res;
  }

  // =========================================================== Add User
  Future<dynamic> addUser(Map<String, dynamic> body) async {
    final res = await _request(
      endPoint: EndPoints.Users,
      requestType: RequestType.Post,
      header: Headers.baseHeader,
      body: body,
    );
    return res;
  }

  // =========================================================== Update User
  static Future<dynamic> updateUser(int id, Map<String, dynamic> body) async {
    final res = await _request(
      endPoint: EndPoints.Users + "/$id",
      requestType: RequestType.Put,
      header: Headers.baseHeader,
      body: body,
    );
    return res;
  }
}
