import 'package:flutter/material.dart';
import 'package:wassity/core/services/core_helper/preference.dart';
import 'package:wassity/view/styles/app_colors.dart';

class AppTheme with ChangeNotifier {
  late bool isDark;
  ThemeMode? themeMode;

  AppTheme() {
    isDark = Preference.getBool(PrefKeys.isDark) ?? false;
    changeThemeMode(Preference.getString(PrefKeys.theme)!);
    print("*** Hello from app theme ***");
  }

  // ======================================= Change Theme (Light & Dark)
  void toogleThemeMode() async {
    isDark = !isDark;
    await Preference.setBool(PrefKeys.isDark, isDark);
    notifyListeners();
    print(isDark);
  }

// ======================================= Change Theme by ThemeMode (Light , Dark & System)
  void changeThemeMode(String theme) {
    switch (theme) {
      case "System default":
        {
          themeMode = ThemeMode.system;
        }
        break;
      case "Light":
        {
          themeMode = ThemeMode.light;
        }
        break;
      case "Dark":
        {
          themeMode = ThemeMode.dark;
        }
        break;
    }
    notifyListeners();
  }

  // ===================================================================================================
  // =========================================== Light Theme ===========================================
  // ===================================================================================================
  final lightTheme = ThemeData(
    fontFamily: "Raleway",
    brightness: Brightness.light,
    primaryColor: AppColors.primaryColor,
    accentColor: AppColors.accentColor,
    backgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    dialogBackgroundColor: Colors.white,
    indicatorColor: AppColors.primaryColor,
    // focusColor: AppColors.primaryColor,
    focusColor: Colors.greenAccent,
    cardColor: Colors.white,
    buttonColor: AppColors.primaryColor,
    bottomAppBarColor: Colors.white,
    dividerColor: Colors.grey,
    hintColor: Colors.grey,
    errorColor: Colors.redAccent,

    // colorScheme: ColorScheme.fromSwatch().copyWith(
    //   primary: AppColors.primaryColor,
    //   secondary: AppColors.accentColor,
    // ),

    // =========================================== AppBar Theme
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.primaryColor,
      titleTextStyle: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16,
        color: Colors.white,
      ),
    ),

    // =========================================== FloatingAction Theme
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: AppColors.accentColor,
    ),

    // =========================================== SnackBar Theme
    snackBarTheme: SnackBarThemeData(
      actionTextColor: AppColors.primaryColor,
      backgroundColor: Color(0xFF323232),
      contentTextStyle: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    ),

    // =========================================== ElevatedButton Theme
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: AppColors.primaryColor,
        textStyle: TextStyle(
          fontSize: 16,
          color: Colors.white,
        ),
      ),
    ),

    // =========================================== Slider Theme
    sliderTheme: SliderThemeData(
      thumbColor: AppColors.primaryColor,
      // overlayColor: AppColors.,
      valueIndicatorColor: Colors.black87,
      activeTrackColor: AppColors.primaryColor,
      // inactiveTrackColor: Colors.pink,
    ),

    // =========================================== TextSelection Theme
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: AppColors.primaryColor,
      selectionHandleColor: AppColors.primaryColor,
    ),
    // =========================================== InputDecoration Theme
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: AppColors.primaryColor,
      ),
      hintStyle: TextStyle(
        color: Colors.grey,
        fontWeight: FontWeight.bold,
      ),

      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: Colors.grey,
        ),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: AppColors.primaryColor,
        ),
      ),
      // disabledBorder: UnderlineInputBorder(
      //   borderSide: BorderSide(
      //     style: BorderStyle.solid,
      //     color: Colors.red,
      //   ),
      // ),
      // border: UnderlineInputBorder(
      //   borderSide: BorderSide(
      //     style: BorderStyle.solid,
      //     color: Colors.green,
      //   ),
      // ),
      // errorBorder: UnderlineInputBorder(
      //   borderSide: BorderSide(
      //     style: BorderStyle.solid,
      //     color: Colors.teal,
      //   ),
      // ),
    ),
    // =========================================== TextButton Theme
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: AppColors.primaryColor,
        textStyle: TextStyle(
          fontSize: 16,
          color: AppColors.primaryColor,
        ),
      ),
    ),

    // =========================================== Icon Theme
    iconTheme: IconThemeData(
      color: AppColors.primaryColor,
    ),
    accentIconTheme: IconThemeData(
      color: Colors.black,
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.black,
    ),

    // =========================================== Text Theme
    textTheme: TextTheme(
      headline6: TextStyle(
        // color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.normal,
      ),
    ),
  );

  // ===================================================================================================
  // =========================================== Dark Theme ============================================
  // ===================================================================================================

  final darkTheme = ThemeData(
    fontFamily: "Raleway",
    brightness: Brightness.dark,
    primaryColor: Colors.blueGrey.shade900,
    accentColor: Colors.blueGrey.shade900,
    backgroundColor: Colors.grey.shade900,
    scaffoldBackgroundColor: Colors.grey.shade900,
    dialogBackgroundColor: Colors.grey.shade900,
    indicatorColor: AppColors.primaryColor,
    cardColor: Colors.blueGrey.shade900,
    buttonColor: AppColors.primaryColor,
    bottomAppBarColor: Colors.grey.shade900,
    dividerColor: Colors.white54,
    hintColor: Colors.grey.shade700,
    errorColor: Colors.redAccent,

    // colorScheme: ColorScheme.fromSwatch().copyWith(
    //   primary: AppColors.primaryColor,
    //   secondary: AppColors.accentColor,
    // ),

    // =========================================== AppBar Theme
    appBarTheme: AppBarTheme(
      backgroundColor: Colors.blueGrey.shade900,
      titleTextStyle: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16,
        color: Colors.grey,
      ),
    ),

    // =========================================== FloatingAction Theme
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.blueGrey.shade900,
    ),

    // =========================================== SnackBar Theme
    snackBarTheme: SnackBarThemeData(
      actionTextColor: AppColors.primaryColor,
      backgroundColor: Colors.grey.shade600,
      contentTextStyle: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    ),

    // =========================================== ElevatedButton Theme
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: Colors.blueGrey.shade900,
        textStyle: TextStyle(
          fontSize: 16,
          color: Colors.white,
        ),
      ),
    ),

    // =========================================== Slider Theme
    sliderTheme: SliderThemeData(
      thumbColor: AppColors.primaryColor,
      // overlayColor: AppColors.,
      valueIndicatorColor: Colors.black87,
      activeTrackColor: AppColors.primaryColor,
      // inactiveTrackColor: Colors.pink,
    ),

    // =========================================== TextSelection Theme
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: AppColors.primaryColor,
      selectionHandleColor: AppColors.primaryColor,
    ),

    // =========================================== InputDecoration Theme
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: AppColors.primaryColor,
      ),
      hintStyle: TextStyle(
        color: Colors.grey,
        fontWeight: FontWeight.bold,
      ),

      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: Colors.grey,
        ),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: AppColors.primaryColor,
        ),
      ),
      // disabledBorder: UnderlineInputBorder(
      //   borderSide: BorderSide(
      //     style: BorderStyle.solid,
      //     color: Colors.red,
      //   ),
      // ),
      // border: UnderlineInputBorder(
      //   borderSide: BorderSide(
      //     style: BorderStyle.solid,
      //     color: Colors.green,
      //   ),
      // ),
      // errorBorder: UnderlineInputBorder(
      //   borderSide: BorderSide(
      //     style: BorderStyle.solid,
      //     color: Colors.teal,
      //   ),
      // ),
    ),

    // =========================================== TextButton Theme
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: Colors.blueGrey.shade900,
        textStyle: TextStyle(
          fontSize: 16,
          color: Colors.white,
        ),
      ),
    ),

    // =========================================== Icon Theme
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    accentIconTheme: IconThemeData(
      color: Colors.white,
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.white,
    ),

    // =========================================== Text Theme
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Colors.grey,
        fontSize: 16,
        fontWeight: FontWeight.normal,
      ),
    ),
  );
}
