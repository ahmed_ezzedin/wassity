import 'package:url_launcher/url_launcher.dart';

enum LaunchType { URL, EMAIL, TEL, SMS }

class Launcher {
//================================================================= Launch URL, EMAIL, TEL, SMS
  static void launcherFun(String target, LaunchType launchType,
      {String? subject, String? body}) async {
    // String target = "http://google.com/";
    var isCanLaunch = await canLaunch(target);
    try {
      switch (launchType) {
        case LaunchType.URL:
          if (isCanLaunch) {
            launch(target);
          } else {
            throw "Could not launch url";
          }
          break;
        case LaunchType.EMAIL:
          launch("mailto:$target?subject=$subject&body=$body");
          break;
        case LaunchType.TEL:
          launch("tel:$target");
          break;
        case LaunchType.SMS:
          launch("sms:$target");
          break;
        default:
          if (isCanLaunch) {
            launch(target);
          } else {
            throw "Could not launch url";
          }
      }
    } catch (error) {
      print(error);
    }
  }

//================================================================= Launch Location on Google Map
  static void googleMapLauncher(
      {required double latitude, required double longitude}) async {
    try {
      String googleUrl =
          'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
      if (await canLaunch(googleUrl)) {
        await launch(googleUrl);
      } else {
        throw 'Could not open the map.';
      }
    } catch (error) {
      print(error);
    }
  }

//================================================================= Launch More App on Google play
  static void launchMoreApp(String developerId) async {
    try {
      String url =
          'https://play.google.com/store/apps/developer?id=$developerId';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    } catch (error) {
      print(error);
    }
  }
}
