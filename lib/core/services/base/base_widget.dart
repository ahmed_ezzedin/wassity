import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum WidgetType { normalBuild, staticBuild, consume }

class BaseWidget<T extends ChangeNotifier> extends StatefulWidget {
  final T? model;
  final Widget? staticChild;
  final WidgetType? type;
  final Function(T)? initState;
  final Function(T)? dispose;
  final Widget Function(BuildContext context, T model, Widget? child) builder;
  final Widget Function(BuildContext context, T model)? staticBuilder;

  BaseWidget({
    this.model,
    this.initState,
    this.dispose,
    this.staticBuilder,
    required this.builder,
  })  : this.staticChild = null,
        this.type = WidgetType.normalBuild;

  BaseWidget.staticBuilder({
    this.model,
    this.initState,
    this.dispose,
    this.staticBuilder,
    required this.builder,
  })  : this.type = WidgetType.staticBuild,
        this.staticChild = null;

  BaseWidget.cosnume({required this.builder})
      : this.type = WidgetType.consume,
        this.model = null,
        this.staticChild = null,
        this.initState = null,
        this.dispose = null,
        this.staticBuilder = null;
  @override
  _BaseWidgetState<T> createState() => _BaseWidgetState<T>();
}

class _BaseWidgetState<T extends ChangeNotifier> extends State<BaseWidget<T>> {
  T? model;
  @override
  void initState() {
    model = widget.model;
    if (widget.initState != null) {
      widget.initState!(model!);
    }
    super.initState();
  }

  @override
  void dispose() {
    model = widget.model;
    if (widget.dispose != null) {
      widget.dispose!(model!);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.type == WidgetType.consume) {
      return Consumer<T>(builder: widget.builder);
    } else {
      return ChangeNotifierProvider<T>(
        create: (context) => model!,
        child: widget.type == WidgetType.staticBuild
            ? widget.staticBuilder!(context, model!)
            : Consumer<T>(child: widget.staticChild, builder: widget.builder),
      );
    }
  }
}
