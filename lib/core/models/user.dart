class User {
  dynamic id;
  int? createAt;
  String? name;
  String? phone;
  String? email;

  User({
    required this.id,
    required this.createAt,
    required this.name,
    required this.phone,
    required this.email,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    createAt = json["createAt"];
    name = json["name"];
    phone = json["phone"];
    email = json["email"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = this.id;
    data["createAt"] = this.createAt;
    data["name"] = this.name;
    data["phone"] = this.phone;
    data["email"] = this.email;
    return data;
  }

  
}
