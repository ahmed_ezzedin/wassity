import 'package:flutter/material.dart';

class AppColors {
// final appTheme = AppTheme.of(context);
  static const Color primaryColor = Color(0xFF01DFF6);
  static const Color accentColor = Color(0xFF01DFF6);
  static const Color lightBackgroundColor = Color(0xFFe3edf7);
  static const Color darkBackgroundColor1 = Color(0xFF191919);
  static const Color darkBackgroundColor2 = Color(0xFF333333);
  static const Color cardColor = Color(0xFFebf5ff);

  static const Color text1 = Color(0xFF55beca);
  static const Color text2 = Color(0xFFAAA6A6);
  static const Color textLight = Colors.white;
  
  // static const Color test2 = Color.fromARGB(255, 171, 20, 28);
  // static const Color test3 = Color.fromARGB(255, 231, 175, 25);
}
