// Class that will hold most of UI Dialogs

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:wassity/view/widgets/dialogs/congrat_dialog.dart';
import 'package:wassity/view/widgets/dialogs/custom_approve_dialog.dart';
import 'package:wassity/view/widgets/dialogs/edit_dialog.dart';
import 'package:wassity/view/widgets/dialogs/error_dialog.dart';
import 'package:wassity/view/widgets/dialogs/exit_dialog.dart';
import 'package:wassity/view/widgets/dialogs/hire_dialog.dart';

class DialogHelper {

  //================================================================= Error Dialog
  static Future<dynamic> errorDialog(BuildContext context,
      {String? message, bool isSuccess = false}) async {
    return await showDialog(
      context: context,
      builder: (ctx) {
        return ErrorDialog(message: message, isSuccess: isSuccess);
      },
    );
  }

//================================================================= Congratulation Dialog
  static Future<dynamic> congratDialog(
    BuildContext context, {
    // Function acceptFun,
    bool isDismissible = true,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: isDismissible,
      builder: (ctx) {
        return CongratulationDialog(context: context);
      },
    );
  }

//================================================================= Hire Dialog
  static Future<dynamic> hireDialog(
    BuildContext context, {
    // Function acceptFun,
    bool isDismissible = true,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: isDismissible,
      builder: (ctx) {
        return HireDialog(context: context);
      },
    );
  }

//================================================================= Edit Dialog
  static Future<dynamic> editDialog(
    BuildContext context, {
    // Function acceptFun,
    bool isDismissible = true,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: isDismissible,
      builder: (ctx) {
        return EditDialog(context: context);
      },
    );
  }

  //================================================================= Custom Approve Dialog
  static Future<dynamic> customApproveDialog(
    BuildContext context,
  ) async {
    return await showDialog(
      context: context,
      builder: (ctx) {
        return CustomApproveDialog(context: context);
      },
    );
  }

  //================================================================= Exit Dialog
  static Future<dynamic> exitDialog(
    BuildContext context,
  ) async {
    return await showDialog(
      context: context,
      builder: (ctx) {
        return ExitDialog(context: context);
      },
    );
  }



}
