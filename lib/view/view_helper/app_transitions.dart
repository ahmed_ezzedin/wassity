import 'package:flutter/material.dart';

enum TransitionType {
  None,
  Scale,
  Size,
  Fade,
  Rotation,
  SlideUp,
  SlideDown,
  SlideLeft,
  SlideRight,
}

class AppTransition<T> extends PageRouteBuilder<T> {
  final TransitionType transitionType;
  final Duration duration;
  final Curve curve;
  final Curve revCurve;
  final Widget page;

  AppTransition({
    required this.transitionType,
    required this.page,
    this.duration = const Duration(milliseconds: 300),
    this.curve = Curves.easeOut,
    this.revCurve = Curves.easeIn,
  }) : super(
          transitionDuration: duration,
          pageBuilder: (context, animation, secAnimation) => page,
          transitionsBuilder:
              (BuildContext context, animation, secAnimation, child) {
            animation = CurvedAnimation(
                parent: animation, curve: curve, reverseCurve: revCurve);

            switch (transitionType) {

              ///=================================== [ None ]
              case TransitionType.None:
                return child;

              ///=================================== [ Scale ]
              case TransitionType.Scale:
                return ScaleTransition(
                  scale: animation,
                  child: child,
                );

              ///=================================== [ Size ]
              case TransitionType.Size:
                return Align(
                  child: SizeTransition(
                    sizeFactor: animation,
                    axis: Axis.horizontal,
                    child: child,
                  ),
                );

              ///=================================== [ Fade ]
              case TransitionType.Fade:
                return FadeTransition(
                  opacity: animation,
                  child: child,
                );

              ///=================================== [ Rotation ]
              case TransitionType.Rotation:
                return RotationTransition(
                  turns: animation,
                  child: child,
                );

              ///=================================== [ SlideUp ]
              case TransitionType.SlideUp:
                return SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(0, 1),
                    end: Offset(0, 0),
                  ).animate(animation),
                  child: child,
                );

              ///=================================== [ SlideDown ]
              case TransitionType.SlideDown:
                return SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(0, -1),
                    end: Offset(0, 0),
                  ).animate(animation),
                  child: child,
                );

              ///=================================== [ SlideLeft ]
              case TransitionType.SlideLeft:
                return SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(1, 0),
                    end: Offset(0, 0),
                  ).animate(animation),
                  child: child,
                );

              ///=================================== [ SlideRight ]
              case TransitionType.SlideRight:
                return SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(-1, 0),
                    end: Offset(0, 0),
                  ).animate(animation),
                  child: child,
                );

              ///=================================== [ default ]
              default:
                return ScaleTransition(
                  scale: animation,
                  alignment: Alignment.topRight,
                  child: child,
                );
            }
          },
        );
}
