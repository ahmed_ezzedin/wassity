import 'package:flutter/material.dart';
import 'package:wassity/view/styles/app_colors.dart';

enum ButtonType { Elevated, Outlined, Text, Icon }

class MainButton extends StatelessWidget {
  final String title;
  final Function()?  onPressed;
  final Color color;
  final ButtonType type;
  final double height;
  final double borderRadius;
  final IconData icon;
  final double iconSize;
  final bool isWithIcon;
  MainButton({
     required this.onPressed,
    this.title = "button title",
    this.color = AppColors.primaryColor,
    this.type = ButtonType.Elevated,
    this.height = 40,
    this.borderRadius = 5,
    this.icon = Icons.android,
    this.iconSize = 24,
    this.isWithIcon = false,
  });
  @override
  Widget build(BuildContext context) {
    switch (type) {

      ///===================================================== [ Elevated ]
      case ButtonType.Elevated:
        {
          return Container(
            height: height,
            width: double.infinity,
            child: isWithIcon
                ? ElevatedButton.icon(
                    icon: Icon(icon),
                    style: ElevatedButton.styleFrom(
                      // primary: color,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius),
                      ),
                    ),
                    label: Text(title),
                    onPressed: onPressed,
                  )
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      // primary: color,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius),
                      ),
                    ),
                    child: Text(
                      title,
                      // style: Theme.of(context).elevatedButtonTheme.style.te
                    ),
                    onPressed: onPressed,
                  ),
          );
        }

      ///===================================================== [ Outlined ]
      case ButtonType.Outlined:
        {
          return Container(
            height: height,
            width: double.infinity,
            child: isWithIcon
                ? OutlinedButton.icon(
                    icon: Icon(icon, color: color),
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(
                          color: color, style: BorderStyle.solid, width: 2),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius),
                      ),
                    ),
                    label: Text(title,
                        style: TextStyle(
                          color: color,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        )),
                    onPressed: onPressed,
                  )
                : OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(
                          color: color, style: BorderStyle.solid, width: 2),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius),
                      ),
                    ),
                    child: Text(title,
                        style: TextStyle(
                          color: color,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        )),
                    onPressed: onPressed,
                  ),
          );
        }

      ///===================================================== [ Text ]
      case ButtonType.Text:
        {
          return Container(
            height: height,
            width: double.infinity,
            child: isWithIcon
                ? TextButton.icon(
                    icon: Icon(icon),
                    style: TextButton.styleFrom(
                      primary: color,
                      textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius),
                      ),
                    ),
                    label: Text(title),
                    onPressed: onPressed,
                  )
                : TextButton(
                    style: TextButton.styleFrom(
                      primary: color,
                      textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius),
                      ),
                    ),
                    child: Text(title),
                    onPressed: onPressed,
                  ),
          );
        }

      ///===================================================== [ Icon ]
      case ButtonType.Icon:
        {
          return IconButton(
            icon: Icon(icon),
            iconSize: iconSize,
            color: color,
            padding: const EdgeInsets.all(8),
            onPressed: onPressed,
          );
        }

      ///===================================================== [ default ]
      default:
        return Text("default!");
    }
  }
}
