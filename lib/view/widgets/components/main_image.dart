import 'package:flutter/material.dart';
import 'dart:io';

import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';

enum ImageType { Network, Asset, File }

class MainImage extends StatelessWidget {
  final ImageType imageType;
  final String imagePath;
  final BoxFit boxFit;
  final double height;
  final double width;
  final double? latitude;
  final double? longitude;
  final double radius;
  final bool isCircle;

  MainImage({
    required this.imageType,
    required this.imagePath,
    this.boxFit = BoxFit.contain,
    this.height = 100,
    this.width = 100,
    this.latitude,
    this.longitude,
    this.radius = 0,
    this.isCircle = false,
  });
  @override
  Widget build(BuildContext conetxt) {
    switch (imageType) {

      ///===================================================== [ Network ]
      case ImageType.Network:
        return Container(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(isCircle ? height / 2 : radius),
            child: Image.network(
              "$imagePath",
              fit: boxFit,
              errorBuilder: (_, obj, trace) => Icon(
                Icons.broken_image,
                size: 50,
                color: AppColors.primaryColor,
              ),
              loadingBuilder: (
                BuildContext context,
                Widget child,
                ImageChunkEvent? loadingProgress,
              ) {
                if (loadingProgress == null) {
                  return child;
                } else {
                  return Center(
                    child: MainProgress(
                      color: AppColors.primaryColor,
                      stroke: 2,
                    ),
                  );
                }
              },
            ),
          ),
        );

      ///===================================================== [ Asset ]
      case ImageType.Asset:
        return Container(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(isCircle ? height / 2 : radius),
            child: Image.asset(
              "$imagePath",
              fit: boxFit,
              errorBuilder: (_, obj, trace) => Icon(
                Icons.broken_image,
                size: 50,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        );

      ///===================================================== [ File ]
      case ImageType.File:
        return Container(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(isCircle ? height / 2 : radius),
            child: Image.file(
              File("$imagePath"),
              fit: boxFit,
              errorBuilder: (_, obj, trace) => Icon(
                Icons.broken_image,
                size: 50,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        );


      ///===================================================== [ default ]
      default:
        return Text("default!");
    }
  }
}
