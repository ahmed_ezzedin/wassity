import 'package:flutter/material.dart';
import 'package:wassity/view/styles/text_styles.dart';

class MainText extends StatelessWidget {
  final String text;
  final TextStyle style;

  const MainText.heading1(this.text) : style = TextStyles.heading1Style;

  @override
  Widget build(BuildContext context) {
    return Text(text, style: style);
  }
}
