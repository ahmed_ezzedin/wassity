import 'package:flutter/material.dart';
import 'package:wassity/view/styles/app_colors.dart';

class MainTextFieldDropdown extends StatelessWidget {
  final List<dynamic> items;
  final dynamic initialValue;
  final String? hint;
  final String? label;
  final bool isFilled;
  final Color? fillColor;
  final EdgeInsetsGeometry? contentPadding;
  final Function(dynamic)? onChanged;

  MainTextFieldDropdown({
    required this.items,
    this.initialValue,
    this.hint,
    this.label,
    this.isFilled = false,
    this.fillColor,
    this.contentPadding,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: DropdownButtonFormField(
        items: items.map((item) {
          return DropdownMenuItem(
            value: item,
            child: Text(item),
          );
        }).toList(),
        value: initialValue,
        onChanged: onChanged,
        icon: Icon(Icons.keyboard_arrow_down),
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          filled: isFilled,
          fillColor: fillColor,
          hintText: hint,
          labelText: label,
          hintStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          labelStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.grey, width: 1.2),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.primaryColor, width: 1.2),
            borderRadius: BorderRadius.circular(10),
          ),
          // in case text field has error and focused in the same time
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.grey, width: 1.2),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.red, width: 1.2),
          ),
        ),
        validator: (value) {
          return value != null ? null : "$label is Reqired!";
        },
      ),
    );
  }
}
