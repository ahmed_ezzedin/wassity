import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';

enum Validator {
  Required,
  Email,
  Mobile,
  Password,
  Number,
  IsMatch,
  MinLength,
  MaxLength,
}

class MainTextField extends StatelessWidget {
  final TextEditingController controller;
  final String? hint;
  final String? label;
  final Widget? icon;
  final bool isObscure;
  final bool isReadOnly;
  final bool isAutofocus;
  final bool isFilled;
  final Color? fillColor;
  final int maxLines;
  final double letterSpacing;
  final TextInputType keyboardType;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final Validator? validator;
  final String? matchedValue;
  final FocusNode? focusNode;
  final TextInputAction textInputAction;
  final EdgeInsetsGeometry? contentPadding;
  final Function()? onTap;
  final Function(String? x)? onChanged;

  MainTextField({
    required this.controller,
    this.hint,
    this.label,
    this.icon,
    this.isObscure = false,
    this.isReadOnly = false,
    this.isAutofocus = false,
    this.isFilled = false,
    this.fillColor,
    this.maxLines = 1,
    this.letterSpacing = 0,
    this.keyboardType = TextInputType.text,
    this.suffixIcon,
    this.prefixIcon,
    this.validator,
    this.matchedValue,
    this.focusNode,
    this.textInputAction = TextInputAction.next,
    this.contentPadding,
    this.onTap,
    this.onChanged,
  });
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    // var tempFocusNode;
    //  focusNode ==null? tempFocusNode = FocusNode(): tempFocusNode = focusNode;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: TextFormField(
        controller: controller,
        cursorWidth: 1.5,
        cursorColor: Colors.red,
        cursorRadius: Radius.circular(5),
        obscureText: isObscure,
        readOnly: isReadOnly,
        keyboardType: keyboardType,
        style: TextStyle(fontSize: 16, letterSpacing: letterSpacing),
        maxLines: maxLines,
        autofocus: isAutofocus,
        // focusNode: tempFocusNode,
        focusNode: focusNode,
        textInputAction: textInputAction,
        decoration: InputDecoration(
          isDense: false,
          filled: isFilled,
          fillColor: fillColor,
          focusColor: Colors.teal,
          icon: icon,
          hintText: hint,
          labelText: label,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          hintStyle: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            letterSpacing: 0,
          ),
          labelStyle: TextStyle(
            // color: AppColors.primaryColor,
            // color: tempFocusNode.hasFocus ? AppColors.primaryColor : Colors.grey,
            fontSize: 15,
            fontWeight: FontWeight.bold,
            letterSpacing: 0,
          ),
          contentPadding: contentPadding ??
              const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          // ======================================================================= Underline Border
          // border: UnderlineInputBorder(
          //   borderSide: BorderSide(color: Colors.grey),
          // ),
          // enabledBorder: UnderlineInputBorder(
          //   borderSide: BorderSide(color: Colors.grey),
          // ),
          // focusedBorder: UnderlineInputBorder(
          //   borderSide: BorderSide(color: AppColors.primaryColor),
          // ),
          // errorBorder: OutlineInputBorder(
          //   borderSide: BorderSide(color: Colors.red),
          // ),
          // ======================================================================= Outline Border
          // in case text field has error and focused in the same time
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(3),),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(3),
            borderSide: BorderSide(color: Colors.grey.shade400, width: 1.2),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(3),
            borderSide: BorderSide(color: AppColors.primaryColor, width: 1.2),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(3),
            borderSide: BorderSide(color: Colors.red, width: 1.2),
          ),
          // =======================================================================
        ),
        validator: (value) {
          switch (validator) {

            ///============================================================================ [Required]
            case Validator.Required:
              if (value!.trim().isEmpty) {
                return locale.get("Rquired");
              }
              return null;

            ///============================================================================ [Email]
            case Validator.Email:
              RegExp regExp = RegExp(
                  r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
                  r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
                  r"{0,253}[a-zA-Z0-9])?)*$");
              if (value!.trim().isEmpty) {
                return locale.get("Please enter email address");
              } else if (!regExp.hasMatch(value)) {
                return locale.get("Please enter valid email address");
              }
              return null;

            ///============================================================================ [Mobile]
            case Validator.Mobile:
              RegExp regExp = RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)');
              if (value!.trim().isEmpty) {
                return locale.get("Please enter mobile number");
              } else if (!regExp.hasMatch(value)) {
                return locale.get("Please enter valid mobile number");
              }
              return null;

            ///============================================================================ [Number]
            case Validator.Number:
              double? number = double.tryParse(value!);
              if (value.trim().isEmpty) {
                return locale.get("Rquired");
              } else if (number == null) {
                return locale.get("Please enter valid number");
              }
              return null;

            ///============================================================================ [Password]
            case Validator.Password:
              if (value!.trim().isEmpty) {
                return locale.get("Please enter your password");
              } else if (value.length < 8) {
                return locale.get("Password is at lest 8 Char");
              }
              return null;

            ///============================================================================ [MinLength]
            case Validator.MinLength:
              if (value!.trim().isEmpty) {
                return locale.get("Rquired");
              } else if (value.length < 8) {
                return locale.get("Minimum length is 8 characters");
              }
              return null;

            ///============================================================================ [MaxLength]
            case Validator.MaxLength:
              if (value!.trim().isEmpty) {
                return locale.get("Rquired");
              } else if (value.length > 12) {
                return locale.get("Maximum length is 12 characters");
              }
              return null;

            ///============================================================================ [IsMatch]
            case Validator.IsMatch:
              if (value!.isEmpty || value != matchedValue) {
                return locale.get("Password not matched");
              }
              return null;

            default:
              return null;
          }
        },
        onTap: onTap,
        onChanged: onChanged,
      ),
    );
  }
}
