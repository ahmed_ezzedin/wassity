import 'package:flutter/material.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/styles/app_colors.dart';

enum ProgressType { Circular, Linear }

class MainProgress extends StatelessWidget {
  final Color color;
  final double diameter;
  final double stroke;
  final ProgressType type;
  final double height;
  final double? linearWidth;

  MainProgress({
    this.color = AppColors.primaryColor,
    this.stroke = 4,
    this.diameter = 35,
    this.height = 4,
    this.linearWidth,
    this.type = ProgressType.Circular,
  });

  @override
  Widget build(BuildContext context) {
    Widget progress;
    switch (type) {
      case ProgressType.Circular:
        progress = Center(
          child: Container(
            height: diameter,
            width: diameter,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(color),
              strokeWidth: stroke,
            ),
          ),
        );
        break;
      case ProgressType.Linear:
        progress = Container(
          width: SizeConfig.width,
          child: LinearProgressIndicator(
            minHeight: height,
            valueColor: AlwaysStoppedAnimation<Color>(color),
          ),
        );
        break;
    }
    return progress;
  }
}
