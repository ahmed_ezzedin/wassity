import 'package:flutter/material.dart';

typedef void RatingChangeCallback(double rating);

class StarsRatingWidget extends StatelessWidget {
  final int starsCount;
  final double rating;
  final double spacing;
  final double iconSize;
  final Function(double)? onRatingChanged;
  final Color color;
  final Color filledColor;
  final isReadOnly;

  StarsRatingWidget({
    this.starsCount = 5,
    this.rating = 0.0,
    this.spacing = 3,
    this.color = Colors.orangeAccent,
    this.filledColor = Colors.grey,
    this.onRatingChanged,
    this.isReadOnly = false,
    this.iconSize = 20,
  });

  Widget buildStars(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = Icon(
        Icons.star_border,
        color: filledColor,
        size: iconSize,
      );
    } else if (index > rating - 1 && index < rating) {
      icon = Icon(
        Icons.star_half,
        color: color,
        size: iconSize,
      );
    } else {
      icon = Icon(
        Icons.star,
        color: color,
        size: iconSize,
      );
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: spacing),
      child: InkResponse(
        child: icon,
        onTap: isReadOnly
            ? null
            : () {
                onRatingChanged!(index + 1.0);
              },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(
        starsCount,
        (index) => buildStars(context, index),
      ),
    );
  }
}
