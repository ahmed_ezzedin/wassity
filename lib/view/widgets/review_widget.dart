import 'package:flutter/material.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/stars_rating_widget.dart';

class ReviewWidget extends StatelessWidget {
  const ReviewWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MainImage(
                  imageType: ImageType.Network,
                  imagePath:
                      'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                  isCircle: true,
                  height: 50,
                  width: 50,
                  boxFit: BoxFit.cover,
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Khaled ahmed',
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      "seo iqlife",
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 12,
                      ),
                    ),
                    Wrap(
                      children: [
                        StarsRatingWidget(
                          spacing: 0,
                          rating: 5,
                          color: Colors.yellow.shade700,
                          iconSize: 10,
                        ),
                        SizedBox(width: 10),
                        Text(
                          "(5/5)",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            Text(
              "I Can Implement, God Willing, And Within The Specified Period. I Have Experience In The Arabic Language And Have Written A Number Of Stories And Technical Topics And Others. Please",
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
      onTap: (){
        DialogHelper.hireDialog(context);
      },
    );
  }
}
