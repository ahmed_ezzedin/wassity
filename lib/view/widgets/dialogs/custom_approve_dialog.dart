import 'package:flutter/material.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';

class CustomApproveDialog extends StatelessWidget {
  final BuildContext context;
  final String title;
  final String subTitle;
  CustomApproveDialog({
    required this.context,
    this.title = "Delete",
    this.subTitle = "Are you sure to remove this item",
  });

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Dialog(
      insetPadding: const EdgeInsets.all(20),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              locale.get("$title"),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(height: 10),
            Text(
              locale.get("$subTitle"),
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                buildTextButton(
                  locale.get("Cnacel"),
                  () {
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(width: 20),
                buildTextButton(
                  locale.get("Confirm"),
                  () {
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  TextButton buildTextButton(String title, Function() onPressed) {
    return TextButton(
      child: Text(
        "$title",
        style: TextStyle(
          fontSize: 13,
          color: AppColors.primaryColor,
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: onPressed,
    );
  }
}
