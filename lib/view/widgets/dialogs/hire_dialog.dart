import 'package:flutter/material.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/stars_rating_widget.dart';

class HireDialog extends StatelessWidget {
  final BuildContext context;
  final String title;
  final String subTitle;
  HireDialog({
    required this.context,
    this.title = "Remove this item",
    this.subTitle = "Are you sure you want delete this item?",
  });

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: const EdgeInsets.all(15),
      content: Container(
        height: SizeConfig.height * 0.7,
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.grey.shade400,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(),
            SizedBox(),
            SizedBox(),
            SizedBox(),
            MainImage(
              imageType: ImageType.Network,
              imagePath:
                  'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
              isCircle: true,
              height: 80,
              width: 80,
              boxFit: BoxFit.cover,
            ),
            Text(
              'Khaled ahmed',
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 26,
              ),
            ),
            Text(
              "CEO IQ2LIFE",
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
              ),
            ),
            Wrap(
              children: [
                StarsRatingWidget(
                  spacing: 0,
                  rating: 5,
                  color: Colors.orange,
                  iconSize: 15,
                ),
                SizedBox(width: 10),
                Text(
                  "(5/5)",
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Text(
              locale.get('Do you want to accept Khaled\'s offer?'),
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.008),
            Text(
              locale.get(
                  'The site will be a mediator between you and Khaled until the transaction is completed'),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(),
            SizedBox(),
            SizedBox(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  child: Text(
                    locale.get("Hire Him"),
                    style: TextStyle(fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    // AppHelper.push(context, OfferStagePage());
                  },
                ),
                ElevatedButton(
                  child: Text(
                    locale.get("Cancel"),
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
            SizedBox(),
          ],
        ),
      ),
    );
  }
}
