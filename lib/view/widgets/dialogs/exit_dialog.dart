import 'package:flutter/material.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';

class ExitDialog extends StatelessWidget {
  final BuildContext context;
  final String title;
  final String subTitle;
  ExitDialog({
    required this.context,
    this.title = "app name",
    this.subTitle = "Are you sure you want to exit?",
  });

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Dialog(
      insetPadding: const EdgeInsets.all(20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
      child: Container(
        padding: const EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              locale.get("$title"),
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            SizedBox(height: 10),
            Text(
              locale.get("$subTitle"),
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                buildTextButton(
                  locale.get("CANCEL"),
                  () {
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(width: 20),
                buildTextButton(
                  locale.get("OK"),
                  () {
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  TextButton buildTextButton(String title, Function() onPressed) {
    return TextButton(
      child: Text(
        "$title",
        style: TextStyle(
          fontSize: 13,
          color: AppColors.primaryColor,
          // fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: onPressed,
    );
  }
}
