import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wassity/core/services/core_helper/file_helper.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';

class EditDialog extends StatelessWidget {
  final BuildContext context;
  final String title;
  final String subTitle;
  EditDialog({
    required this.context,
    this.title = "Remove this item",
    this.subTitle = "Are you sure you want delete this item?",
  });

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      insetPadding: const EdgeInsets.all(10),
      content: Container(
        height: SizeConfig.height * 0.7,
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.grey.shade400,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            InkWell(
              child: Stack(
                children: [
                  MainImage(
                    imageType: ImageType.Network,
                    imagePath:
                        'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                    isCircle: true,
                    height: 100,
                    width: 100,
                    boxFit: BoxFit.cover,
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      color: Colors.black45,
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      Icons.camera_alt_outlined,
                      color: Colors.white,
                      size: 40,
                    ),
                  ),
                ],
              ),
              onTap: () async {
                final result = await FileHelper.pickImage(ImageSource.gallery);
                if (result != null) {
                  print("result");
                }
              },
            ),
            SizedBox(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    SvgPicture.asset("assets/svgs/pin.svg"),
                    SizedBox(width: 10),
                    Text(
                      "From Egypt",
                      style: TextStyle(fontSize: 12),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.edit_outlined,
                        size: 22,
                        color: Colors.grey,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      "CEO IQ2LIFE",
                      style: TextStyle(fontSize: 12),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.edit_outlined,
                        size: 22,
                        color: Colors.grey,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(),
            SizedBox(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      locale.get("Brief About Me"),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.edit_outlined,
                        size: 22,
                        color: Colors.grey,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
                Text(
                  "Hi. I Am Khaled, I Have A Bachelor’s Degree In Information Systems, And I Have Been Working As A User Interface And User Experience Designer For Two Years, And I Have Many Projects That I Designed. I Consider Your Design As My Design Because It Simply Speaks About Me And The Degree Of Professionalism For My Work. I Can Do The Following Professionally And Quickly I Have Experience In Hi. I Am Sherif Ashraf, I Have A Bachelor’s Degree In Information Systems, And I Have Been Working As A User Interface And User Experience Designer For Two Years, And I Have Many Projects That I Designed. I Consider Your Design As My Design Because It Simply Speaks A",
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            SizedBox(),
            SizedBox(),
          ],
        ),
      ),
    );
  }
}
