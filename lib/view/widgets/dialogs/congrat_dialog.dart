import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/offers_stage_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class CongratulationDialog extends StatelessWidget {
  final BuildContext context;
  final String title;
  final String subTitle;
  CongratulationDialog({
    required this.context,
    this.title = "Remove this item",
    this.subTitle = "Are you sure you want delete this item?",
  });

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: const EdgeInsets.all(20),
      content: Container(
        height: SizeConfig.height * 0.7,
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(),
            SizedBox(),
            SizedBox(),
            SizedBox(),
            SvgPicture.asset("assets/svgs/tick.svg"),
            Text(
              locale.get('Congratulations'),
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(),
            Text(
              locale.get(
                  'Your Deal Has Been Successfully Added.. Please Wait For The Brokers To Place Their Offers On Your Deal'),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(),
            SizedBox(),
            SizedBox(),
            ElevatedButton(
              child: Text(
                locale.get("Add A New Deal"),
                style: TextStyle(fontSize: 16),
              ),
              onPressed: () {
                // Navigator.of(context).pop();
                AppHelper.push(context, OfferStagePage());
              },
            ),
          ],
        ),
      ),
    );
  }
}
