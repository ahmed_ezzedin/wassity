
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';

class MyProjectsItem extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
  final AppLocalizations locale = AppLocalizations.of(context);
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 7),
      height: SizeConfig.height * 0.15,
      width: double.infinity,
      child: Row(
        children: [
          MainImage(
            height: SizeConfig.height * 0.15,
            width: SizeConfig.height * 0.15,
            imageType: ImageType.Network,
            radius: 5,
            boxFit: BoxFit.cover,
            imagePath:
                "https://www.miamiherald.com/latest-news/lxz0cj/picture250860979/alternates/FREE_1140/Best%20Crypto%20Exchange.jpg",
          ),
          SizedBox(width: 10),
          Column(
            mainAxisAlignment:
                MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Coin Dealer Wanted',
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 16,
                ),
              ),
              Wrap(
                alignment: WrapAlignment.spaceBetween,
                // crossAxisAlignment: CrossAxisAlignment.,
                children: [
                  Wrap(
                    children: [
                      Icon(
                        Icons.person_outlined,
                        size: 15,
                      ),
                      Text(
                        "Sherif Ashraf",
                        style: TextStyle(
                          color: Colors.black45,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(width: 10),
                  Wrap(
                    children: [
                      Icon(
                        Icons.access_time,
                        size: 15,
                      ),
                      Text(
                        "Hour Ago",
                        style: TextStyle(
                          color: Colors.black45,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  color:
                      AppColors.primaryColor.withAlpha(20),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      locale.get("50"),
                      style: TextStyle(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SvgPicture.asset(
                      "assets/svgs/save-money.svg",
                      color: AppColors.primaryColor,
                      height: 17,
                      width: 17,
                    ),
                    // SizedBox(width: 10),
                    Text(
                      locale.get("Offer"),
                      style: TextStyle(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}