import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<AboutPageModel>(
      model: AboutPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: Text(locale.get("About")),
          ),
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MainImage(
                    height: SizeConfig.height * 0.2,
                    width: double.infinity,
                    imageType: ImageType.Network,
                    radius: 5,
                    boxFit: BoxFit.cover,
                    imagePath:
                        "http://cryptoincome.io/wp-content/uploads/2018/01/DQmbtsqci8T9j9oNrQg4BxXr8hSsLYseiY788HuN7i7LfZu.jpg",
                  ),
                  Text(
                    "About Waseety",
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Waseati is the first Arab market for buying and selling currencies. Waseati brings together the Arab youth who are ready to provide services, and the category of buyers who are willing to buy these services, thus providing a suitable income for Arab youth and distinguished services at an economical price for individuals and startups. Waseati was launched in August 2021 by iqlife, and the site won first place in the Technology World Competition for the Best Arab Web Projects for 2011. Hassoub announced its purchase of Khamsat on 1/7/2012. My broker is affiliated with a company, registered in the UK with number 07571594.",
                  ),
                  Center(
                      child: Stack(
                    alignment: Alignment.center,
                    children: [
                      SvgPicture.asset("assets/svgs/Group 290.svg"),
                      Image.asset(
                        "assets/images/app_logo.png",
                        height: SizeConfig.width * 0.20,
                        width: SizeConfig.width * 0.20,
                      ),
                    ],
                  )),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class AboutPageModel extends BaseModel {
  final BuildContext context;

  AboutPageModel({required this.context});
}
