import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/review_widget.dart';

class OfferStagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<OfferStagePageModel>(
      model: OfferStagePageModel(context: context),
      builder: (_, model, child) {
        final locale = AppLocalizations.of(context);
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: SizeConfig.height * 0.03),
                    Text(
                      locale.get("Start Building Your Deal"),
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 24,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.01),
                    Wrap(
                      alignment: WrapAlignment.center,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      runAlignment: WrapAlignment.center,
                      children: [
                        Text(
                          locale.get("We We Currency Exchange Deal"),
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                        SizedBox(width: 10),
                        SvgPicture.asset("assets/svgs/money (1).svg"),
                      ],
                    ),
                    SizedBox(height: SizeConfig.height * 0.05),
                    Theme(
                      data: ThemeData(
                        canvasColor: AppColors.primaryColor,
                        accentColor: AppColors.primaryColor,
                        primaryColor: AppColors.primaryColor,
                      ),
                      child: Stepper(
                        controlsBuilder: (
                          BuildContext context, {
                          VoidCallback? onStepContinue,
                          VoidCallback? onStepCancel,
                        }) {
                          return SizedBox();
                        },
                        type: StepperType.vertical,
                        steps: [
                          Step(
                              title:
                                  Text(locale.get("Stage Of Receiving Offers")),
                              content: SizedBox(),
                              isActive: true,
                              state: StepState.complete),
                          Step(
                            title: Text(locale.get("Implementation Phase")),
                            content: SizedBox(),
                          ),
                          Step(
                            title: Text(locale.get("Delivery Stage")),
                            content: SizedBox(),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.05),
                    Text(
                      locale.get("Transaction Status"),
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      locale.get("Project Details"),
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Text(
                      locale.get(
                          "Digital Currency (Digital Currency And Digital Money) Is A Type Of Digital Currency And Digital Cash. Physical, Physical, Portable, Portable, Portable, Portable, Portable, Portable Mutual, Foreign Exchange Exchange. On The Same Front, You Can Use Currencies In Forex Trading. Digital Currency (Digital Currency And Digital Money) Is A Type Of Digital Currency And Digital Cash. Physical, Physi"),
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Text(
                      locale.get("Offers On Your Deal"),
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Column(
                      children: model.reviewList.map(
                        (e) {
                          return ReviewWidget();
                        },
                      ).toList(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class OfferStagePageModel extends BaseModel {
  final BuildContext context;
  OfferStagePageModel({required this.context});
  List<String> reviewList = [
    "",
    "",
    "",
  ];

  int currentStep = 0;
  Map<String, dynamic> stepperMap = {};
}
