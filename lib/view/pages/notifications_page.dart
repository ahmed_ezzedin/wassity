import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/widgets/components/main_image.dart';

class NotificationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<NotificationsPageModel>(
      model: NotificationsPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: Text(locale.get("Notifications")),
          ),
          body: Container(
            padding: const EdgeInsets.all(10),
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: MainImage(
                    imageType: ImageType.Network,
                    imagePath:
                        'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                    isCircle: true,
                    height: 50,
                    width: 50,
                    boxFit: BoxFit.cover,
                  ),
                  title: Text(
                    "You Were Chosen By Khaled To Complete His Deal",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                  subtitle: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Icon(
                        Icons.access_time_rounded,
                        color: Colors.grey,
                      ),
                      SizedBox(width: 10),
                      Text(
                        "2 Days And 11 Minutes",
                        style: TextStyle(
                          fontWeight: FontWeight.w500 ,
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}

class NotificationsPageModel extends BaseModel {
  final BuildContext context;

  NotificationsPageModel({required this.context});
}
