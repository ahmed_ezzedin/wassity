import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_text.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';

class AddDealPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddDealPageModel>(
      model: AddDealPageModel(context: context),
      builder: (_, model, child) {
        final locale = AppLocalizations.of(context);
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Form(
                autovalidateMode: model.autovalidateMode,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: SizeConfig.height * 0.03),
                      Text(
                        locale.get("Start Building Your Deal"),
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 24,
                        ),
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      Wrap(
                        alignment: WrapAlignment.center,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        runAlignment: WrapAlignment.center,
                        children: [
                          Text(
                            locale.get(
                                "Start Your Project On Waseati Platform Quickly"),
                            style: TextStyle(
                              // fontWeight: FontWeight.w900,
                              // fontSize: 24,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(width: 10),
                          SvgPicture.asset("assets/svgs/money (1).svg"),
                        ],
                      ),
                      SizedBox(height: SizeConfig.height * 0.05),
                      MainText.heading1(locale.get("Project title")),
                      SizedBox(height: SizeConfig.height * 0.005),
                      MainTextField(
                        controller: model.titleController,
                        hint: locale.get("Project title"),
                        validator: Validator.Required,
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      MainText.heading1(
                          locale.get("The Amount to be transferred")),
                      SizedBox(height: SizeConfig.height * 0.005),
                      MainTextField(
                        controller: model.amountController,
                        hint: locale.get("The Amount to be transferred"),
                        validator: Validator.Number,
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      MainText.heading1(
                          locale.get("Details of the transactions")),
                      SizedBox(height: SizeConfig.height * 0.005),
                      MainTextField(
                        controller: model.detailsController,
                        hint: locale.get("Details of the transactions"),
                        validator: Validator.Required,
                        maxLines: 10,
                      ),
                      SizedBox(height: SizeConfig.height * 0.05),
                      MainButton(
                        title: locale.get("Add A New Deal"),
                        onPressed: () {
                          // AppHelper.pushReplaceAll(context, HomeNavPage());
                          DialogHelper.congratDialog(context);
                        },
                      ),
                      SizedBox(height: SizeConfig.height * 0.02),
                   
                      
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class AddDealPageModel extends BaseModel {
  final BuildContext context;

  AddDealPageModel({required this.context});

  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final titleController = TextEditingController();
  final amountController = TextEditingController();
  final detailsController = TextEditingController();
  bool? rememberMe = false;

  void submitFun() {
    if (formKey.currentState!.validate()) {
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }
}
