import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';

class ZZZPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ZZZPageModel>(
      model: ZZZPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(title: Text("test Page")),
          body: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  "Empty Page",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class ZZZPageModel extends BaseModel {
  final BuildContext context;

  ZZZPageModel({required this.context});
}
