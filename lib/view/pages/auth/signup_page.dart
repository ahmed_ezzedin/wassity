import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_text.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:wassity/view/widgets/language_buttons.dart';
import 'package:wassity/view/widgets/login_with_buttons.dart';

class SignupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<SignupPageModel>(
      model: SignupPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(15),
              width: double.infinity,
              child: SingleChildScrollView(
                child: Form(
                  key: model.formKey,
                  autovalidateMode: model.autovalidateMode,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      Positioned(
                        top: -100,
                        left: -100,
                        child: SvgPicture.asset("assets/svgs/Group 290.svg"),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Column(
                              children: [
                                LanguageButtons(),
                                SizedBox(height: SizeConfig.height * 0.07),
                                Image.asset(
                                  "assets/images/app_logo.png",
                                  height: SizeConfig.width * 0.25,
                                  width: SizeConfig.width * 0.25,
                                ),
                                SizedBox(height: SizeConfig.height * 0.03),
                                Text(
                                  locale.get("SIGN UP"),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // SizedBox(height: SizeConfig.height * 0.15),
                          MainText.heading1(locale.get("Your Name")),
                          SizedBox(height: SizeConfig.height * 0.005),
                          MainTextField(
                            controller: model.emailController,
                            hint: "Your name",
                            suffixIcon: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Icon(Icons.person_outline)),
                            validator: Validator.Required,
                          ),
                          MainText.heading1(locale.get("Email Address")),
                          SizedBox(height: SizeConfig.height * 0.005),
                          MainTextField(
                            controller: model.emailController,
                            hint: "Sherifzosar@Gmail.Com",
                            suffixIcon: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Icon(Icons.email_outlined)),
                            validator: Validator.Email,
                          ),
                          SizedBox(height: SizeConfig.height * 0.01),
                          MainText.heading1(locale.get("Password")),
                          SizedBox(height: SizeConfig.height * 0.005),
                          MainTextField(
                            controller: model.emailController,
                            hint: "****************",
                            suffixIcon: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Icon(Icons.lock_outline_rounded),
                            ),
                            validator: Validator.Password,
                          ),

                          SizedBox(height: SizeConfig.height * 0.05),
                          MainButton(
                            title: locale.get("Sign up"),
                            onPressed: model.submitFun,
                          ),
                          LoginWithButtons(),

                          Center(
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: AppColors.primaryColor))),
                              child: Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  Text(
                                    locale.get("Have Account?"),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                    ),
                                  ),
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      minimumSize: Size.zero,
                                      // padding: EdgeInsets.zero,
                                      tapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                    ),
                                    child: Text(
                                      locale.get("Login "),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12,
                                      ),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class SignupPageModel extends BaseModel {
  final BuildContext context;

  SignupPageModel({required this.context});
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool? rememberMe = false;

  void submitFun() {
    if (formKey.currentState!.validate()) {
      print(emailController.text);
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }
}
