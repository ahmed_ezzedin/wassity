import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/language_buttons.dart';

class PasswordChangedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<PasswordChangedPageModel>(
      model: PasswordChangedPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(15),
              width: double.infinity,
              child: SingleChildScrollView(
                child: Form(
                  key: model.formKey,
                  autovalidateMode: model.autovalidateMode,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      Positioned(
                        top: -100,
                        left: -100,
                        child: SvgPicture.asset("assets/svgs/Group 290.svg"),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Column(
                              children: [
                                LanguageButtons(),
                                SizedBox(height: SizeConfig.height * 0.07),
                                Image.asset(
                                  "assets/images/app_logo.png",
                                  height: SizeConfig.width * 0.25,
                                  width: SizeConfig.width * 0.25,
                                ),
                                SizedBox(height: SizeConfig.height * 0.03),
                                Text(
                                  locale.get("PASSWORD CHANGED"),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(height: SizeConfig.height * 0.015),
                                Text(
                                  locale.get(
                                      "As A Security Measure, We Will Redirect You To The Login Page So That We Will Save Your New Password"),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: SizeConfig.height * 0.25),
                          MainButton(
                            title: locale.get("Send"),
                            onPressed: model.submitFun,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class PasswordChangedPageModel extends BaseModel {
  final BuildContext context;

  PasswordChangedPageModel({required this.context});
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  void submitFun() {
    if (formKey.currentState!.validate()) {
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }
}
