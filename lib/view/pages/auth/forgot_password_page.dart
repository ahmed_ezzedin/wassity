import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/auth/new_password_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_text.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:wassity/view/widgets/language_buttons.dart';

class ForgetPasswordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<ForgetPasswordPageModel>(
      model: ForgetPasswordPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(15),
              width: double.infinity,
              child: SingleChildScrollView(
                child: Form(
                  key: model.formKey,
                  autovalidateMode: model.autovalidateMode,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      Positioned(
                        top: -100,
                        left: -100,
                        child: SvgPicture.asset("assets/svgs/Group 290.svg"),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Column(
                              children: [
                                LanguageButtons(),
                                SizedBox(height: SizeConfig.height * 0.07),
                                Image.asset(
                                  "assets/images/app_logo.png",
                                  height: SizeConfig.width * 0.25,
                                  width: SizeConfig.width * 0.25,
                                ),
                                SizedBox(height: SizeConfig.height * 0.03),
                                Text(
                                  locale.get("FORGOT PASSWORD"),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                Container(
                                  width: SizeConfig.width * 0.7,
                                  child: Text(
                                    locale.get(
                                        "Please Write Your Email So We Can Send You  A Password Reset Link"),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: SizeConfig.height * 0.15),
                          MainText.heading1(locale.get("Email Address")),
                          SizedBox(height: SizeConfig.height * 0.005),
                          MainTextField(
                            controller: model.emailController,
                            hint: "Sherifzosar@Gmail.Com",
                            suffixIcon: Padding(
                              padding: const EdgeInsets.all(15),
                              child: SvgPicture.asset("assets/svgs/mail.svg"),
                            ),
                            validator: Validator.Email,
                          ),
                          SizedBox(height: SizeConfig.height * 0.15),
                          MainButton(
                            title: locale.get("Send"),
                            // onPressed: model.submitFun,
                            onPressed: () {
                              AppHelper.push(context, NewPasswordPage());
                            },
                          ),
                          SizedBox(height: SizeConfig.height * 0.05),
                          Center(
                            child: TextButton(
                              child: Wrap(
                                children: [
                                  SvgPicture.asset("assets/svgs/mail (2).svg"),
                                  SizedBox(width: 10),
                                  Text(
                                    locale.get("Resend Email"),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () {},
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class ForgetPasswordPageModel extends BaseModel {
  final BuildContext context;

  ForgetPasswordPageModel({required this.context});
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final emailController = TextEditingController();

  void submitFun() {
    if (formKey.currentState!.validate()) {
      print(emailController.text);
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }
}
