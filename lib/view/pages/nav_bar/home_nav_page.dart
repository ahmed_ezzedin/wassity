import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/drawer_page.dart';
import 'package:wassity/view/pages/messages_page.dart';
import 'package:wassity/view/pages/nav_bar/cart_tab.dart';
import 'package:wassity/view/pages/nav_bar/favorite_tab.dart';
import 'package:wassity/view/pages/nav_bar/home_tab.dart';
import 'package:wassity/view/pages/nav_bar/profile_tab.dart';
import 'package:wassity/view/pages/notifications_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/app_helper.dart';

class HomeNavPage extends StatelessWidget {
  final List<Widget> listTabs = [
    HomeTab(),
    CartTab(),
    FavoriteTab(),
    ProfileTab(),
  ];

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<HomeNavPageModel>(
      model: HomeNavPageModel(context: context, locale: locale),
      builder: (_, model, child) {
        return Scaffold(
          key: model.scaffoldKey,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            leading: Padding(
              padding: const EdgeInsets.all(10),
              child: IconButton(
                icon: SvgPicture.asset("assets/svgs/Group 309.svg"),
                onPressed: () {
                  model.scaffoldKey.currentState!.openDrawer();
                },
              ),
            ),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.notifications_outlined,
                  color: Colors.grey,
                ),
                onPressed: () {
                  AppHelper.push(context, NotificationsPage());
                },
              ),
              IconButton(
                icon: Icon(
                  Icons.email_outlined,
                  color: Colors.grey,
                ),
                onPressed: () {
                  AppHelper.push(context, MessagesPage());
                },
              ),
            ],
          ),
          drawer: DrawerPage(model: model),
          body: listTabs[model.pageIndex],
          bottomNavigationBar: AnimatedContainer(
            duration: Duration(milliseconds: 400),
            height: model.isVisible ? 55 : 0,
            child: Wrap(
              children: [
                BottomNavigationBar(
                  // items: navigationBarItem(model),
                  items: [
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.home_outlined,
                        color: model.pageIndex == 0
                            ? AppColors.primaryColor
                            : Colors.grey,
                      ),
                      label: locale.get('Home'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shopping_bag_outlined,
                        color: model.pageIndex == 1
                            ? AppColors.primaryColor
                            : Colors.grey,
                      ),
                      label: locale.get('Cart'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.favorite_outline,
                        color: model.pageIndex == 2
                            ? AppColors.primaryColor
                            : Colors.grey,
                      ),
                      label: locale.get('Favorite'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.person_outline_rounded,
                        color: model.pageIndex == 3
                            ? AppColors.primaryColor
                            : Colors.grey,
                      ),
                      label: locale.get('Profile'),
                    ),
                  ],
                  elevation: 0,
                  currentIndex: model.pageIndex,
                  selectedItemColor: AppColors.primaryColor,
                  unselectedItemColor: Colors.black,
                  showUnselectedLabels: true,
                  showSelectedLabels: true,
                  selectedLabelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColors.primaryColor,
                  ),
                  unselectedLabelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                  onTap: model.changePageIndex,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class HomeNavPageModel extends BaseModel {
  final BuildContext context;
  final AppLocalizations locale;
  HomeNavPageModel({required this.context, required this.locale});

  final TextEditingController searchController = TextEditingController();

  int pageIndex = 0;
  bool isVisible = true;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  changePageIndex(int index) {
    print(index);
    pageIndex = index;
    setState();
  }
}
