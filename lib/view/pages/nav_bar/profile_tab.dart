import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/stars_rating_widget.dart';
import 'package:wassity/view/widgets/review_widget.dart';

class ProfileTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProfileTabModel>(
      model: ProfileTabModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: Container(
            width: double.infinity,
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: SizeConfig.height * 0.01),
                    Align(
                      alignment: Alignment.topRight,
                      child: ElevatedButton(
                        child: Text("Edit"),
                        onPressed: () {
                          DialogHelper.editDialog(context);
                        },
                      ),
                    ),
                    MainImage(
                      imageType: ImageType.Network,
                      imagePath:
                          'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                      isCircle: true,
                      height: 80,
                      width: 80,
                      boxFit: BoxFit.cover,
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Text(
                      'Khaled ahmed',
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 26,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.03),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Wrap(
                          children: [
                            SvgPicture.asset("assets/svgs/pin.svg"),
                            SizedBox(width: 10),
                            Text(
                              "From Egypt",
                              style: TextStyle(
                                color: Colors.grey.shade700,
                                fontWeight: FontWeight.w500,
                                // fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            // SvgPicture.asset("assets/svgs/Repeat Grid 1.svg"),
                            // SvgPicture.asset("assets/svgs/pin.svg"),
                            Icon(
                              Icons.star,
                              color: Colors.yellow.shade600,
                              size: 20,
                            ),
                            SizedBox(width: 10),
                            Text(
                              "5/5",
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                // fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          "CEO IQ2LIFE",
                          style: TextStyle(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w900,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Icon(
                          Icons.access_time,
                          color: Colors.grey,
                          // size: 20,
                        ),
                        SizedBox(width: 10),
                        Text(
                          "Avg. Response Time  1h",
                          style: TextStyle(
                            color: Colors.grey.shade700,
                            fontWeight: FontWeight.w500,
                            // fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: SizeConfig.height * 0.05),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Brief About Me",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.01),
                        Text(
                          "Hi. I Am Khaled, I Have A Bachelor’s Degree In Information Systems, And I Have Been Working As A User Interface And User Experience Designer For Two Years, And I Have Many Projects That I Designed. I Consider Your Design As My Design Because It Simply Speaks About Me And The Degree Of Professionalism For My Work. I Can Do The Following Professionally And Quickly I Have Experience In Hi. I Am Sherif Ashraf, I Have A Bachelor’s Degree In Information Systems, And I Have Been Working As A User Interface And User Experience Designer For Two Years, And I Have Many Projects That I Designed. I Consider Your Design As My Design Because It Simply Speaks A",
                          style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.05),
                        Text(
                          "Customer Reviews And Opinions",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.01),
                        Wrap(
                          children: [
                            StarsRatingWidget(
                              spacing: 0,
                              rating: 5,
                              color: Colors.yellow.shade700,
                              iconSize: 20,
                            ),
                            SizedBox(width: 10),
                            Text("( 5/5 )"),
                            SizedBox(width: 10),
                            Text("( 700 Rating )"),
                          ],
                        ),
                        SizedBox(height: SizeConfig.height * 0.01),
                        Column(
                          children: List.generate(5, (index) => ReviewWidget()),
                        ),
                        SizedBox(height: SizeConfig.height * 0.02),
                        Center(
                          child: TextButton(
                            child: Text(
                              "Show more",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                            onPressed: () {},
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.02),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class ProfileTabModel extends BaseModel {
  final BuildContext context;
  ProfileTabModel({required this.context});
}
