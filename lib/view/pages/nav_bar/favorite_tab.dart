import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';

class FavoriteTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<FavoriteTabModel>(
      model: FavoriteTabModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: Container(
            width: double.infinity,
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Favorite Tab"),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class FavoriteTabModel extends BaseModel {
  final BuildContext context;
  FavoriteTabModel({required this.context});
}
