import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';

class CartTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartTabModel>(
      model: CartTabModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: Container(
            width: double.infinity,
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Cart Tab"),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class CartTabModel extends BaseModel {
  final BuildContext context;
  CartTabModel({required this.context});
}
