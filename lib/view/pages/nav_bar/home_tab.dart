import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/add_deail_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_dropdown_button.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/my_project_item.dart';

class HomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeTabModel>(
      model: HomeTabModel(context: context),
      builder: (_, model, child) {
        final locale = AppLocalizations.of(context);
        return Scaffold(
          body: Container(
            width: double.infinity,
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    locale.get("Currency Exchange"),
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(height: SizeConfig.height * 0.01),
                  Wrap(
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    runAlignment: WrapAlignment.center,
                    children: [
                      Text(
                        locale.get("Exchange Your Currency Faster"), 
                        style: TextStyle(
                          // fontWeight: FontWeight.w900,
                          // fontSize: 24,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(width: 10),
                      SvgPicture.asset("assets/svgs/money (1).svg"),
                    ],
                  ),
                  SizedBox(height: SizeConfig.height * 0.05),
                  buildSwitchButtons(model, locale),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                        child: Wrap(
                          children: [
                            SvgPicture.asset("assets/svgs/add-button.svg"),
                            SizedBox(width: 10),
                            Text(
                              locale.get("Add A Transfer Request"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          AppHelper.push(context, AddDealPage());
                        },
                      ),
                      MainDropdownButton(
                        value: model.selectedValue,
                        items: model.sortList,
                        onChanged: (value) {
                          model.selectedValue = value;
                          model.setState();
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: SizeConfig.height * 0.02),
                  model.isMyProj
                      ? Expanded(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return MyProjectsItem();
                            },
                          ),
                        )
                      : Expanded(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return Container(
                                margin: const EdgeInsets.symmetric(vertical: 7),
                                height: SizeConfig.height * 0.15,
                                width: double.infinity,
                                child: Row(
                                  children: [
                                    MainImage(
                                      height: SizeConfig.height * 0.15,
                                      width: SizeConfig.height * 0.15,
                                      imageType: ImageType.Network,
                                      radius: 5,
                                      boxFit: BoxFit.cover,
                                      imagePath:
                                          "http://cryptoincome.io/wp-content/uploads/2018/01/DQmbtsqci8T9j9oNrQg4BxXr8hSsLYseiY788HuN7i7LfZu.jpg",
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Wrap(
                                            crossAxisAlignment:
                                                WrapCrossAlignment.center,
                                            children: [
                                              MainImage(
                                                imageType: ImageType.Network,
                                                imagePath:
                                                    'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                                                isCircle: true,
                                                height: 40,
                                                width: 40,
                                                boxFit: BoxFit.cover,
                                              ),
                                              SizedBox(width: 10),
                                              Text(
                                                'Khaled ahmed',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w900,
                                                  fontSize: 16,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            'I Will Buy A We We Coin From You At The Largest Price And Sell It At The Largest Price',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.black87,
                                            ),
                                          ),
                                          Wrap(
                                            crossAxisAlignment:
                                                WrapCrossAlignment.center,
                                            children: [
                                              Wrap(
                                                children: [
                                                  Icon(
                                                    Icons.star,
                                                    size: 15,
                                                    color:
                                                        Colors.yellow.shade600,
                                                  ),
                                                  Text(
                                                    "5",
                                                    style: TextStyle(
                                                      color: Colors
                                                          .yellow.shade600,
                                                      fontSize: 10,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  SizedBox(width: 10),
                                                  Text(
                                                    "(120)",
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      // fontWeight:
                                                      //     FontWeight.bold,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(width: 10),
                                              Wrap(
                                                children: [
                                                  Text(
                                                    "starting from ",
                                                    style: TextStyle(
                                                      color: AppColors
                                                          .primaryColor,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  Text(
                                                    "20 \$",
                                                    style: TextStyle(
                                                      color: Colors
                                                          .yellow.shade600,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Row buildSwitchButtons(HomeTabModel model, AppLocalizations locale) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
          child: Column(
            children: [
              Container(
                width: SizeConfig.width * 0.4,
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  color: model.isMyProj
                      ? AppColors.primaryColor.withAlpha(50)
                      : Colors.grey.withAlpha(50),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Wrap(
                  children: [
                    SvgPicture.asset("assets/svgs/Group 206.svg"),
                    SizedBox(width: 10),
                    Text(
                      locale.get("My Projects"),
                      style: TextStyle(
                        color: model.isMyProj
                            ? AppColors.primaryColor
                            : Colors.grey,
                        // fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              model.isMyProj
                  ? Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.primaryColor,
                    )
                  : SizedBox(height: 24),
            ],
          ),
          onTap: () {
            model.isMyProj = true;
            model.setState();
          },
        ),
        SizedBox(),
        InkWell(
          child: Column(
            children: [
              Container(
                width: SizeConfig.width * 0.4,
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  color: model.isMyProj
                      ? Colors.grey.withAlpha(70)
                      : AppColors.primaryColor.withAlpha(50),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Wrap(
                  children: [
                    SvgPicture.asset("assets/svgs/Repeat Grid 2.svg"),
                    SizedBox(width: 10),
                    Text(
                      locale.get("Mediators"),
                      style: TextStyle(
                        color: model.isMyProj
                            ? Colors.grey
                            : AppColors.primaryColor,
                        // fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              model.isMyProj
                  ? SizedBox(height: 24)
                  : Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.primaryColor,
                    ),
            ],
          ),
          onTap: () {
            model.isMyProj = false;
            model.setState();
          },
        ),
      ],
    );
  }
}

class HomeTabModel extends BaseModel {
  final BuildContext context;
  HomeTabModel({required this.context});
  bool isMyProj = true;
  String selectedValue = "New";
  List<String> sortList = [
    "New",
    "Highest rated",
    "Old",
  ];
}
