import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/view/pages/auth/signin_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class SplashPage extends StatelessWidget {
  // final NotificationAppLaunchDetails notificationAppLaunchDetails;
  // SplashPage(this.notificationAppLaunchDetails);
  // static const String routeName = '/';
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashPageModel>(
      model: SplashPageModel(context: context),
      builder: (context, model, child) {
        return Scaffold(
          body: Container(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    "assets/images/app_logo.png",
                    height: SizeConfig.width * 0.25,
                    width: SizeConfig.width * 0.25,
                  ),
                  SizedBox(height: 30),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class SplashPageModel extends BaseModel {
  final BuildContext context;
  SplashPageModel({required this.context}) {
    delayFun();
  }

  void delayFun() {
    Future.delayed(
      Duration(milliseconds: 2000),
      () {
        AppHelper.pushReplaceAll(context, SigninPage());
      },
    );
  }
}
