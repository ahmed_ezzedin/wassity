
import 'package:flutter/material.dart';
import 'package:wassity/view/pages/about_page.dart';
import 'package:wassity/view/pages/nav_bar/home_nav_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({required this.model});

  final HomeNavPageModel model;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 150,
            width: double.infinity,
            color: Colors.grey,
            child: Center(
              child: Image.asset(
                "assets/images/app_logo.png",
                height: SizeConfig.width * 0.15,
                width: SizeConfig.width * 0.15,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            title: Text(model.locale.get("About")),
            onTap: () {
              AppHelper.push(context, AboutPage());
            },
          ),
        ],
      ),
    );
  }
}
