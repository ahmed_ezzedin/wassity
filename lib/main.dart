import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:wassity/core/services/core_helper/preference.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/core/services/theme/app_theme.dart';
import 'package:wassity/view/pages/splash_page.dart';
import 'package:wassity/view/view_helper/size_config.dart';

void main() async {
  /// ============================= Initialize before calling [runApp]
  WidgetsFlutterBinding.ensureInitialized();

  /// ============================= Initialize Preference
  await Preference.initialize();

  /// ============================= Initialize Firebase
  // await Firebase.initializeApp();

  /// ============================= Initialize Firebase Notification
  // await FirebaseNotificationService().initialize();

  /// ============================= Setup Git it (Locator)
  setupLocator();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        /// ============================= Initialize Size Config
        SizeConfig().initialize(constraints, orientation);
        return MultiProvider(
          providers: providers,
          child: Consumer2<AppLanguage, AppTheme>(
            builder: (context, language, theme, _) => MaterialApp(
              title: 'My Templet',
              debugShowCheckedModeBanner: false,
              // theme: theme.isDark ? theme.darkTheme : theme.lightTheme,
              theme: theme.lightTheme,
              darkTheme: theme.darkTheme,
              themeMode: theme.themeMode,
              home: SplashPage(),
              locale: language.appLocale,
              supportedLocales: [
                Locale("en"),
                Locale("ar"),
              ],
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
            ),
          ),
        );
      });
    });
  }
}